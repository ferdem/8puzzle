

// Encapsulates a search strategy
public abstract class Search {
	
	// Encapsulates a node of the search tree
	public static class Node {
		Board board;
		Integer eval; // evaluation value 
		Integer tile; // tile that is moved 
		Integer cost; // level of the node

		// Constructs a new node
		public Node(Board board, Integer eval, Integer tile, Integer cost) {
			this.board = board;
			this.eval = eval;
			this.tile = tile;
			this.cost = cost;
		}
	}
	
	protected Node curr; // current node which is being considered
	
	// statistics
	protected int maxMem; // maximum number of nodes kept in the memory
	protected int maxDepth; // maximum depth explored during the search process
	protected long time; // total time it takes to reach the solution
	protected int steps; // steps to reach the solution
	protected int depth; // depth of the solution

	// constructs a new search with initial state init 
	public Search(int[][] init) {
		curr = new Node(new Board(init), 0, 0, 0);	
		
		maxMem = 0;
		maxDepth = 0;
		steps = 0;
	}
	
	// starts the search
	public void start() {
		long start = System.nanoTime();
		
		search();
		
		time = System.nanoTime() - start;
		depth = curr.cost;
		
		printStats();
	}
	
	// prints the perfermance measures
	protected void printStats() {
		System.out.println("a) " + maxMem +
				"\nb) " + maxDepth +
				"\nc) " + time +
				"\nd) " + steps +
				"\ne) " + depth + "\n");
	}
	
	// search algorithm
	protected abstract void search(); 
	
	// evaluation function
	protected abstract int eval(Board moved);
		
}
