
// same test in the report
public class Test {
	
	// sample games
	static int[][] m1 = {
		{ 1, 2, 5 },
		{ 3, 0, 4 },
		{ 6, 7, 8 }
	};

	static int[][] m2 = { 
		{ 4, 2, 5 },
		{ 1, 6, 7 },
		{ 3, 0, 8 } 
	};
	
	public static void main(String[] args) {
		
		System.out.println("BFS on m1");
		BFS bfs1 = new BFS(m1);
		bfs1.start();
		
		System.out.println("BFS on m2");
		BFS bfs2 = new BFS(m2);
		bfs2.start();
		
		System.out.println("DFS on m1");
		DFS dfs = new DFS(m1);
		dfs.start();
			
		System.out.println("Iterative deepening on m1");
		IterativeDeepening id1 = new IterativeDeepening(m1);
		id1.start();
		
		System.out.println("Iterative deepening on m2");
		IterativeDeepening id2 = new IterativeDeepening(m2);
		id2.start();
		
		System.out.println("A* with Misplaced Tiles Heuristic on m1");
		AStar a1h1 = new AStar(m1, AStar.Heuristic.MISPLACED);
		a1h1.start();
		
		System.out.println("A* with Misplaced Tiles Heuristic on m2");
		AStar a2h1 = new AStar(m2, AStar.Heuristic.MISPLACED);
		a2h1.start();
		
		System.out.println("A* with Manhattan Distance Heuristic Heuristic on m1");
		AStar a1h2 = new AStar(m1, AStar.Heuristic.MANHATTAN);
		a1h2.start();
		
		System.out.println("A* with Misplaced Tiles Heuristic on m2");
		AStar a2h2 = new AStar(m2, AStar.Heuristic.MANHATTAN);
		a2h2.start();
	}
	
}
