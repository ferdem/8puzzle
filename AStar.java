import java.util.PriorityQueue;

/* A* Search Implementation
 * Class extends from the BFS and everythings is same except
 * it uses a priority queue instead of a linked list as the queue,
 * and evaluation function depends on chosen heuristic function. */
public class AStar extends BFS {

	// Heuristic Functions
	public static enum Heuristic {
		MISPLACED, // number of misplaced tiles 
		MANHATTAN  // total manhattan distance
	}
	
	private Heuristic heuristic; 
	
	// Creates a new A* with heuristic function h
	public AStar(int[][] init, Heuristic h) {
		super(init);
		heuristic = h;
		frontier = new PriorityQueue<>(100, 
				(a, b) -> a.eval.compareTo(b.eval));
		// priority queue uses the evaluation function as comparator
	}

	/* Evaluation function: f(n) = g(n) + h(n)
	 * where g(n) is the cost of moving to the node
	 * and h(n) is heuristic function chosen 
	 * when creating the object.*/
	protected int eval(Board b) {
		return heuristic == Heuristic.MISPLACED ? curr.cost + b.numberOfMisplacedTiles() : 
			curr.cost + b.totalManhattanDistance();
	}

}
