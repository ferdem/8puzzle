
import java.util.Arrays;

/* Encapsulates a board of 8-puzzle */
public class Board {
	
	public int[][] matr; // 3 x 3 matrix to hold the configuration of the board
	
	/* Constructs a board with initial state init with 
	 * deep copy. */
	public Board(int[][] init) {
		matr = new int[3][];
		for (int i = 0; i < 3; i++)
			matr[i] = Arrays.copyOf(init[i], 3);
	}
	
	/* Returns the number of tiles out of place. */
	public int numberOfMisplacedTiles() {
	    int counter = 0;
	    for (int i = 0; i < 3; i++) 
	    	for (int j = 0; j < 3; j++) 
	    		if (matr[i][j] != 3 * i + j)
	    			counter++;
	    return counter;
	}
	
	/* Returns the total Manhattan distance of each tile to its goal */
	public int totalManhattanDistance() {
		int sum = 0;
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (matr[i][j] == 0)
					continue;
				
				int x = matr[i][j] / 3;
				int y = matr[i][j] % 3; 
				
				sum += Math.abs(x - i) + Math.abs(y - j);
			}
		}
		return sum;
	}
	
	/* Moves the tile to the empty place. It doesn't check 
	 * if the tile is next to the empty place. */
	public void move(int t) {
		for (int i = 0; i < 3; i++)  
			for (int j = 0; j < 3; j++) 
				if (matr[i][j] == t) 
					matr[i][j] = 0;
				else if (matr[i][j] == 0) 
					matr[i][j] = t;
	}
	
	/* Returns the board with this.move(i) */
	public Board moved(int i) {
		Board moved = new Board(matr);
		moved.move(i);
		return moved;
	}
	
	/* Prints the board */
	public void print() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++)
				if (matr[i][j] == 0)
					System.out.print("  ");
				else
					System.out.print(matr[i][j] + " ");
			System.out.println();
		}
		System.out.println();
	}
	
	/* Checks if the goal state is reached */
	public boolean isGoalStateReached() {
		return numberOfMisplacedTiles() == 0;
	}
	
	/* It's necesarry for the priority queue. */
	public boolean equals(Object o) {
		Board b = (Board)o;
		boolean f = true;
		for (int i = 0; i < 3; i++) 
			for (int j = 0; j < 3; j++) 
				f &= b.matr[i][j] == matr[i][j];
		return f;
	}
	
}
