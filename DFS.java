import java.util.Stack;

/* Depth First Search */
public class DFS extends Search {

	// A stack is used for the frontier
	public Stack<Node> frontier;
	
	public DFS(int[][] init) {
		super(init);
		frontier = new Stack<>();
	}
		
	@Override
	protected void search() {
		if (curr.board.isGoalStateReached())
			return;
		steps++;
		
		if (frontier.size() > maxMem)
			maxMem = frontier.size();
		
		expandFrontier();
		
		curr = frontier.pop();
		
		if (curr.cost > maxDepth)
			maxDepth = curr.cost;
		
		search();
	}

	// Adds possible moves to the frontier, to be considered
	protected void expandFrontier() {
		int[][] m = curr.board.matr;
		LOOP: for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (m[i][j] == 0) {
					if (i + 1 < 3 && m[i+1][j] != curr.tile) {
						Board moved = curr.board.moved(m[i+1][j]);
						frontier.push(new Node(moved, 0, m[i+1][j], curr.cost + 1));
					}
					if (i - 1 >= 0 && m[i-1][j] != curr.tile) {
						Board moved = curr.board.moved(m[i-1][j]);
						frontier.push(new Node(moved, 0, m[i-1][j], curr.cost + 1));
					}
					if (j + 1 < 3 && m[i][j+1] != curr.tile) {
						Board moved = curr.board.moved(m[i][j+1]);
						frontier.push(new Node(moved, 0, m[i][j+1], curr.cost + 1));
					}
					if (j - 1 >= 0 && m[i][j-1] != curr.tile) {
						Board moved = curr.board.moved(m[i][j-1]);
						frontier.push(new Node(moved, 0, m[i][j-1], curr.cost + 1));
					}
					break LOOP;
				}
			}
		}
	}

	// Evaluation function is not used
	protected int eval(Board moved) {
		return 0;
	}

}
