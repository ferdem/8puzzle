
/* Depth Limited Search
 * This class is only written for IterativeDeepening class.
 * Extends from the DFS and only differs in the sense that 
 * it has limit for the depth of the search. */
public class DepthLimited extends DFS {

	protected int limit;
	protected boolean solved; // is the solution found
	
	// Creates a new Depth Limited with limit l 
	public DepthLimited(int[][] init, int l) {
		super(init);
		limit = l;
	}
	
	public void start() {
		search();
	}
	
	protected void search() {
		if (curr.board.isGoalStateReached()) {
			solved = true;
			return;
		}
		steps++;
		
		expandFrontier();
		
		if (frontier.size() > maxMem)
			maxMem = frontier.size();
		
		curr = frontier.pop();
		
		while (curr.cost > limit && !frontier.empty())
			curr = frontier.pop();
		
		if(curr.cost <= limit) 
			search();
	}
	
}
