
/* Iterative Deepening Depth First Search */
public class IterativeDeepening extends Search {

	public IterativeDeepening(int[][] init) {
		super(init);
	}
	
	/* Creates depth limited search objects with increasing limits 
	 * until it finds the solution. */
	protected void search() {
		for (int i = 1; ;i++) {
			DepthLimited d = new DepthLimited(curr.board.matr, i);
			d.start();
			
			if (d.maxMem > maxMem)
				maxMem = d.maxMem;
			
			steps += d.steps;
			
			if (d.solved) {
				maxDepth = i;
				curr.cost = i;
				break;
			}
		}
	}

	// evaluatin function is not used 
	protected int eval(Board moved) {
		return 0;
	}
	
}
