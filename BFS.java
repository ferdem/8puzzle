import java.util.LinkedList;
import java.util.Queue;

/* Bredth First Search */
public class BFS extends Search {

	// A queue is used for the frontier
	protected Queue<Node> frontier; 
	
	// A linked list is used for the frontier
	public BFS(int[][] init) {
		super(init);
		frontier = new LinkedList<>();
	}

	protected void search() {
		if (curr.board.isGoalStateReached())
			return;
		
		// counts the number of steps
		steps++;
		
		// keeps the max size of frontier
		if (frontier.size() > maxMem)
			maxMem = frontier.size();
		
		expandFrontier();
		curr = frontier.poll();
		
		// keeps the max depth
		if (curr.cost > maxDepth)
			maxDepth = curr.cost;
		
		search();
	}

	// Adds possible moves to the frontier, to be considered
	protected void expandFrontier() {
		int[][] m = curr.board.matr;
		LOOP: for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (m[i][j] == 0) {
					if (j - 1 >= 0 && m[i][j-1] != curr.tile) {
						Board moved = curr.board.moved(m[i][j-1]);
						frontier.add(new Node(moved, eval(moved), m[i][j-1], curr.cost + 1));
					}
					if (j + 1 < 3 && m[i][j+1] != curr.tile) {
						Board moved = curr.board.moved(m[i][j+1]);
						frontier.add(new Node(moved, eval(moved), m[i][j+1], curr.cost + 1));
					}
					if (i - 1 >= 0 && m[i-1][j] != curr.tile) {
						Board moved = curr.board.moved(m[i-1][j]);
						frontier.add(new Node(moved, eval(moved), m[i-1][j], curr.cost + 1));
					}
					if (i + 1 < 3 && m[i+1][j] != curr.tile) {
						Board moved = curr.board.moved(m[i+1][j]);
						frontier.add(new Node(moved, eval(moved), m[i+1][j], curr.cost + 1));
					}
					break LOOP;
				}
			}
		}
	}
	
	// evaluation function is not used 
	protected int eval(Board b) {
		return 0;
	}

}
